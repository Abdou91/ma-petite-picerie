$(document).ready(function() {
    var events = $('#events');
    var total_produit = 0
    var output_table = $('#output_table').DataTable({
          select: {
              style: 'single'
          },
          "dom":"ftip",
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "bAutoWidth": false
      });

    output_table.on( 'select',function ( e, dt, type, indexes ) {
      var rowData = output_table.rows( indexes ).data();
      output_table.row('.selected').remove().draw( false );
      total_produit = total_produit - parseInt(rowData[0][1])
      document.getElementById("total_produit_selectionnes").innerHTML = total_produit + "€";
      });
    var table = $('#example').DataTable({
      columns: [
                  {"data": "nom"},
                  {"data": "quantite"},
                  {"data": "prix"},
              ],
          select: {
              style: 'single'
          },
          "dom":"ftip",
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "bAutoWidth": false
      });
      table.on( 'select',function ( e, dt, type, indexes ) {
      var rowData = table.rows( indexes ).data();
      
      output_table.row.add( [
      rowData[0]['nom'],
      rowData[0]['prix']
        ] ).draw( false );
      qte_updated = parseInt(rowData[0]['quantite']) - 1;
      total_produit = total_produit + parseInt(rowData[0]['prix'])
      document.getElementById("total_produit_selectionnes").innerHTML = total_produit + "€";
      events.prepend( '<div><b>'+type+' selection</b> - '+JSON.stringify( rowData )+'</div>' );
        });
    
        
  });
function payer() {
  var total_a_payer = parseInt(document.getElementById("total_produit_selectionnes").innerHTML);
  if (total_a_payer > 10) {
    msg = "Veuillez supprimer des articles de votre panier, votre budget est insuffisant !";
  } else {
    msg = "Féliciations pour votre achat !";
  }
  alert(msg);
  return msg
}